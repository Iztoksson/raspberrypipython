import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT)
try:
	GPIO.output(23, GPIO.HIGH)
	time.sleep(1)
	GPIO.output(23, GPIO.LOW)
except KeyboardInterrup:
	print "User exit!"
finally:
	GPIO.cleanup()

