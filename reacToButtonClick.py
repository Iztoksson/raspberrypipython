import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

def button_click(channel):
	print("Button pressed!")
	GPIO.setup(7, GPIO.OUT)
	GPIO.output(7, True)
	time.sleep(1)
	GPIO.output(7, False)

GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(10, GPIO.RISING, callback=button_click)

try:
	message = input("Press Enter to quit\n\n")
except KeyboardInterrupt:
	print ("User exit!")
finally:
	GPIO.cleanup()

#try:
#	while True:
#		if GPIO.input(10) == GPIO.HIGH:
#			print ("Button pressed!")
#except KeyboardInterrupt:
#	print ("User exit!")
#finally:
#	GPIO.cleanup()
